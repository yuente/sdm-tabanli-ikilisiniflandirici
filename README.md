# SDM Tabanlı İkili Sınıflandırıcı #

### Bu kodun amacı? ###

Sonlu durum makinesi (SDM) tabanlı ikili sınıflandırıcının kaynak kodudur.
Aşağıdaki bildiride yer alan sonuçlar bu kaynak kod kullanılarak üretilmiştir.


### Nasıl düzenlendi? ###

Kod dizini aşağıdaki şekilde yapılandırılmıştır.

* Classifiers: Bu dizinde sınıflandırıcılar bulunmaktadır. SDM sınıflandırıcısının kaynak kodu da bu dizin altındadır. Bu dizin altında SDM'nin yanısıra YSA (Yapay Sinir Ağları) ve İKA (İkili Karar Ağaçları) için scriptler bulunmaktadır.

* Datasets: Kullanılan verisetleri bu dizin altında tutulmaktadır.

* Utilities: Performans ölçme, verisetini hazırlama gibi yardımcı fonksiyonlar bu dizinde bulunmaktadır.

* Structure: Eğitim sonucunda oluşan SDM tabanlı sınıflandırıcı bu dizinde saklanmaktadır.  

### Nasıl çalıştırılır? ###
Testi başlatmak için benchmark dosyasını MATLAB'den çift tıklamanız ya da MATLAB konsolundan benchmark komutunu yazmanız gerekir.

### İletişim ###

yusufengin@gmail.com