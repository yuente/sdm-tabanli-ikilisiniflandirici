%
% Copyright (c) 2017 Yusuf Engin Tetik
%
%
% benchmark.m 
% Bu kod MATLAB R2016a'da yaz�lm��t�r. 
% �al��t�r�ld���nda a�a��dakine benzer bir ��kt� �retmesi beklenmektedir.

% A�a��daki ��kt�, ba�ta adlar� verilen verisetleri i�in YSA, IKA ve SDM 
% s�n�fland�r�c�lar�n�n HO (Hata Oran�), DHO (Dengeli Hata Oran�),
% KSN (Kesinlik), DYR (Duyarl�l�k) ve F-Skoru performans �l��tlerine
% g�re elde ettikleri sonu�lar� g�stermektedir.

% diabetes
% 			HO		DHO		KSN		DYR		F-S
% 	SDM		0.253	0.315	0.701	0.480	0.565
% 	IKA		0.292	0.318	0.578	0.591	0.579
% 	YSA		0.245	0.286	0.678	0.581	0.619
% 
% ionosphere
% 			HO		DHO		KSN		DYR		F-S
% 	SDM		0.117	0.111	0.861	0.960	0.900
% 	IKA		0.137	0.123	0.873	0.897	0.881
% 	YSA		0.120	0.118	0.841	0.971	0.897
% 
% spambase
% 			HO		DHO		KSN		DYR		F-S
% 	SDM		0.084	0.092	0.914	0.871	0.891
% 	IKA		0.084	0.087	0.890	0.899	0.894
% 	YSA		0.091	0.100	0.906	0.858	0.880

clc;
clear all;

datasets = {
              'diabetes'... 
             ,'ionosphere'... 
             ,'spambase'...            
            };

learners = {
            ,'SDM'... 
            ,'IKA'...
            ,'YSA'...           
            };

addpath('classifiers');
addpath('classifiers/SDM');
addpath('datasets');
addpath('structure');
addpath('utilities');
        
for d = 1 : length(datasets)
    
   data = prepareDataset(datasets{d});
   N = 10; % cross validation partition count
   packets = prepareCVPackets(data.sc, N);
   
   fprintf('\n%s\n',datasets{d});
   fprintf('\t\t\tHO\t\tDHO\t\tKSN\t\tDYR\t\tF-S\n');   
   
   for c = 1 : length(learners)              
       for i = 1 : N
            test_samples = packets(i,:);
            train_samples = [];
            for j = 1:N
                if i ~= j
                    train_samples = cat(2, train_samples, packets(j,:) );        
                end
            end
            TRNX = data.X(train_samples,:);
            TRNY = data.Y(train_samples);           
            TSTX = data.X(test_samples,:);   
            TSTY = data.Y(test_samples);
            PY = feval(learners{c}, TRNX, TRNY, TSTX);
            err(i) = measureError(TSTY, PY);        
       end
     er = mean([err(:).er]);
    ber = mean([err(:).ber]);
    pre = mean([err(:).precision]);
    rec = mean([err(:).recall]);
    fsc = mean([err(:).fscore]);    
    fprintf('\t%s\t\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\n',learners{c}, er, ber,pre, rec, fsc);
   end
    
 
end