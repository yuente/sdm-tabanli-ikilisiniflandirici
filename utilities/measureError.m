function err = measureError(TY, PY)

tp = sum((TY == 1) & (PY == 1));
fn = sum((TY == 1) & (PY == 0));
tn = sum((TY == 0) & (PY == 0));
fp = sum((TY == 0) & (PY == 1));

if any([tp fn tn fp])
          ber = 0.5*( fn/(tp+fn+((tp+fn)==0)) + fp/(fp+tn+((fp+tn)==0))); 
           er = (fn+fp);
           er = er / ( er + tp+tn);
    precision = tp / (tp+fp);
       recall = tp / (tp+fn);
       fscore = (2*precision * recall)/ ( precision + recall);
      %  fprintf('er=%.3f  ber=%.3f  pre=%.3f  rec=%.3f  fsc=%.3f\n',er, ber,precision, recall, fscore);
else
    fprintf('error, all zeros, possible input output mismatch\n');
    ber = inf;
end

err = struct('er',er,'ber',ber,'precision',precision,'recall',recall,'fscore',fscore);

