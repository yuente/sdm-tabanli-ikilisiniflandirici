function data = prepareDataset(dataset)

    switch dataset
        case 'diabetes'
            A = importdata('datasets\pima-indians-diabetes.data');
            [sc, fc] = size(A);
            fc = fc - 1; % since the last column is for labels
            X = A(:,1:fc);
            Y = A(:,fc+1); % since the last column is for labels  

        case 'ionosphere'
            load ionosphere
            [sc, fc] = size(X);
            Y = char(Y) == 'g';

        case 'spambase'
            load datasets\spambase.data
            load datasets\spambase_rndorder.mat         
            X = spambase(spambase_rndorder,1:57);
            Y = spambase(spambase_rndorder,58);        
            [sc, fc] = size(X); 

    end
    data = struct('X',X,'Y',Y, 'sc', sc, 'fc', fc);
    
end