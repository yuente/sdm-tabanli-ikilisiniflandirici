function packets = prepareCVPackets(sc, N)

    elm_order = 1:sc;%   randperm(sc);
    packet_length = floor(sc/N);
    c = 0;
    for i = 1:N
        for j = 1:packet_length
            c = c + 1;
            packets(i,j) = elm_order(c);
        end
    end