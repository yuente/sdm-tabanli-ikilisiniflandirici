function PY = YSA(TRNX, TRNY, TSTX)
    
    TRNX = TRNX';
    TRNY = TRNY';
    TSTX = TSTX';
    
    rng(123);
    fc = size(TRNX,2); % feature count
    myann = patternnet([round(fc*2/3)]);
    myann.trainParam.showWindow=0; 
    
    myann = train(myann, TRNX, TRNY); 
    PY = sim(myann,TSTX);
    PY = round(PY)';
    
end


