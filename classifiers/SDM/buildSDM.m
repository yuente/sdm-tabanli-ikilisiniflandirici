function SDM = buildSDM( X, Y )

 [sc, fc] = size(X);
 
stumps = [];
for i = 1:fc    
    MV = isnan(X(:,i)); % handle missing values 
    stumps(i,:) = trainAttributeClassifier(X(~MV,i),Y(~MV), 1);
    L = attributeClassifier(stumps(i,:),X((~MV),i));
    err(i) = sum(L ~= Y(~MV));
end   

[serr, feature_order]=sort(err);

for i = 1:fc        
 %   fprintf('%g. err:%g\n',feature_order(i), serr(i)/sc);
end   


feature_order = feature_order(1:fc); % you can take first n items of features

fc = length(feature_order);
mynode(1) = State;
nc = 2;
for i = 1:sc 
    n = 1;
    L = Y(i)+1;
    mynode(n).value(L) = mynode(n).value(L) + 1;
    for j = feature_order
        if isnan(X(i,j))
            decision = mynode(n).value(2) > mynode(n).value(1);
        else
            decision = attributeClassifier(stumps(j,:),X(i,j)) ;
        end        
%        mynode(n).edgevalue(decision+1) = mynode(n).edgevalue(decision+1) + 1;
        if decision 
            if  mynode(n).right
                x = mynode(n).right;
                mynode(x).value(L) = mynode(x).value(L) + 1;
                n = x;
            else
                mynode(nc) = State;
                mynode(nc).value(L) = uint32(1);
                mynode(nc).parent = uint32(n);
                mynode(n).right = uint32(nc);
                n = nc;
                nc = nc + 1;                
            end                 
        else
            if  mynode(n).left
                x = mynode(n).left;
                mynode(x).value(L) = mynode(x).value(L) + 1;
                n = x;
            else
                mynode(nc) = State;
                mynode(nc).value(L) = uint32(1);
                mynode(nc).parent = uint32(n);
                mynode(n).left = uint32(nc);                
                n = nc;
                nc = nc + 1;                         
            end                                 
        end
       
    end            
end

SDM = struct('feature_order',feature_order, 'stumps',stumps,'mynode',mynode);


end

