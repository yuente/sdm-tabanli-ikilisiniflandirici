function PY = predictSDM( X, SDM )
    
    [sc, fc] = size(X);
    
    PY = zeros(sc,1);   
    c= 0;
    mynode = SDM.mynode;    
    for i = 1:sc
        n = 1;
        c  = c + 1;        
        zeroscore = double(mynode(n).value(1)) / double(mynode(n).value(1)+mynode(n).value(2));
        onescore = double(mynode(n).value(2)) / double(mynode(n).value(1)+mynode(n).value(2));
        for j = SDM.feature_order 
            if isnan(X(i,j))
                decision = mynode(n).value(2) > mynode(n).value(1);
            else
                decision = attributeClassifier(SDM.stumps(j,:),X(i,j)) ;
            end
            if decision
                n = mynode(n).right;
            else
                n = mynode(n).left;                      
            end            
            if n == 0
                break;
            end

            if 0 < n && n < length(mynode)+1
                zerovalue =  mynode(n).value(1);
                onevalue = mynode(n).value(2);
            else
                zerovalue = 0;
                onevalue = 0;
                fprintf('warning: unexpected situation\n');
            end
            
            if zerovalue > 0 && onevalue > 0              
                 onescore =  onescore + double(onevalue)  / double(onevalue+zerovalue);
                zeroscore = zeroscore + double(zerovalue) / double(onevalue+zerovalue);
            elseif zerovalue > 0
                    zeroscore = zeroscore + 1;
            else
                    onescore = onescore + 1;
            end

        end     
        if onescore > zeroscore
            PY(c) = 1;
        else
            PY(c) = 0;
        end

       % fprintf('1:%g\t 0:%g\t p:%g\t t:%g \n',onescore,zeroscore,decision,TSTY(i) );
    end

end
