 function PY = SDM(TRNX, TRNY, TSTX )
    SDM = buildSDM(TRNX, TRNY);
    PY  = predictSDM(TSTX, SDM);
    save structure/SDM.mat SDM
end