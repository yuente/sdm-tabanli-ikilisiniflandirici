classdef State
    properties
        left = uint32(0);
        right = uint32(0);
        value = uint32([0; 0]);
        edgevalue = uint32([0; 0]);
        parent = uint32(0);
    end
end