function PY = IKA(TRNX, TRNY, TSTX )
    mycart = classregtree(TRNX, TRNY, 'method','classification');    
    PY = eval(mycart,TSTX);   
    PY = sscanf(sprintf('%s*', PY{:}), '%f*');
end
